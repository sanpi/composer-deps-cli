<?php

namespace Sanpi\Composer\Dependency;

use \Symfony\Component\Config\FileLocator;
use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\Console\Application as BaseApplication;
use \Symfony\Component\DependencyInjection\Loader\PhpFileLoader;

class Application extends BaseApplication
{
    public function __construct()
    {
        parent::__construct('Composer dependencies cli');

        $container = new ContainerBuilder();
        $loader = new PhpFileLoader($container, new FileLocator(__DIR__));
        $loader->load('services.php');

        foreach ($container->findTaggedServiceIds('command') as $id => $attributes) {
            $this->add(
                $container->get($id)
            );
        }
    }
}
