<?php

use \Symfony\Component\Yaml\Yaml;
use \Symfony\Component\DependencyInjection\Reference;

if (!is_file('config.yml')) {
    $cwd = getcwd();
    throw new \RuntimeException(
        "Unable to find the configuration file config.yml in $cwd"
    );
}

$config = Yaml::parse('config.yml');
$container->getParameterBag()
    ->add($config);

$container->register('dependencies-analyzer', '\JMS\Composer\DependencyAnalyzer');

$container->register('find-command', '\Sanpi\Composer\Dependency\Command\FindCommand')
    ->addTag('command')
    ->addArgument('%repositories%')
    ->addArgument(new Reference('dependencies-analyzer'));
