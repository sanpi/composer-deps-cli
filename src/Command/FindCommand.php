<?php

namespace Sanpi\Composer\Dependency\Command;

use \Symfony\Component\Console\Command\Command;
use \Symfony\Component\Console\Input\InputOption;
use \Symfony\Component\Console\Input\InputArgument;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;

class FindCommand extends Command
{
    private $repositories;
    private $dependenciesAnalizer;

    public function __construct($repositories, $dependenciesAnalizer)
    {
        parent::__construct();

        $this->repositories = $repositories;
        $this->dependenciesAnalizer = $dependenciesAnalizer;
    }

    protected function configure()
    {
        $this->setName('find')
            ->setDescription('Find where a package is required')
            ->addArgument('package', InputArgument::REQUIRED, 'A package name')
            ->addOption('site', 's', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Specify site(s) to analysis')
            ->addOption('branch', 'b', InputOption::VALUE_OPTIONAL, 'Specify git branch to analysis', 'HEAD');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $packageName = $input->getArgument('package');
        $branch = $input->getOption('branch');
        $sites = $input->getOption('site');
        if (empty($sites)) {
            $sites = array_keys($this->repositories);
        }

        foreach ($sites as $site) {
            try {
                $package = $this->getPackage($site, $branch, $packageName);
                $this->printSite($output, $site, $package);
            }
            catch (\RuntimeException $e) {
                if ($output->isDebug()) {
                    $output->writeln("# $site\n");
                    $output->writeln("<error>{$e->getMessage()}</error>\n");
                }
            }
        }
    }

    private function getPackage($site, $branch, $name)
    {
        if (!isset($this->repositories[$site])) {
            throw new \RuntimeException("Unknow repositories for '$site'");
        }

        $repository = $this->repositories[$site];
        if (is_string($repository)) {
            $repository = [
                'url' => $repository,
                'subdir' => '.',
            ];
        }

        $composerData = $this->gitGet($repository, $branch, 'composer.json');
        $lockData = $this->gitGet($repository, $branch, 'composer.lock');

        $tree = $this->dependenciesAnalizer
            ->analyzeComposerData($composerData, $lockData);

        $package = $tree->getPackage($name);
        if ($package === null) {
            throw new \RuntimeException("Package '$name' not found");
        }

        return $package;
    }

    private function gitGet($repository, $branch, $filename)
    {
        $remote = $repository['url'];
        $dirname = $repository['subdir'];
        if ($dirname === '.') {
            $dirname = '';
        }

        $cmd = "git archive --format=tar --remote=$remote $branch:$dirname $filename | tar x -O";
        $process = new \Symfony\Component\Process\Process($cmd);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new \RuntimeException("Unable to download $remote:$filename");
        }
        return $process->getOutput();
    }

    private function printSite($output, $site, $package)
    {
        $output->writeln("# $site");
        $output->writeln('');

        $version = $package->getVersion();
        if ($version === null) {
            $version = '?';
        }

        $output->writeln("Installed version : <comment>$version</comment>");

        if ($output->isVerbose()) {
            $output->writeln("\nRequired by:");
            foreach ($package->getInEdges() as $edge) {
                $output->write("  - <info>{$edge->getSourcePackage()->getName()}</info> (<comment>{$edge->getVersionConstraint()}</comment>)");
                if ($edge->isDevDependency()) {
                    $output->write(" <question>dev</question>");
                }
                $output->writeln('');
            }
        }
        $output->writeln('');
    }
}
