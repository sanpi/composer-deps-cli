# Composer dependencies CLI

Don’t read the composer.lock, use the CLI, luke!

## Installation

    $ git clone https://github.com/sanpii/composer-deps-cli.git
    $ cd composer-deps-cli
    $ wget http://getcomposer.org/installer -O - | php
    $ ./composer.phar install
    $ cp config.yml{-dist,}

## Usage

    $ ./bin/composer-deps find symfony/event-dispatcher -v
    # composer-deps-cli

    Installed version : v2.4.4

    Dependencies :
      - cpliakas/git-wrapper (~2.0)
      - symfony/console (~2.1)

Verbosity option:

* By defaut, display installed version ;
* ``-v``: display required by packages ;
* ``-vvv``: display errors.
